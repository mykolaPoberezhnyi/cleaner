var gulp = require('gulp'), 
    scss = require('gulp-sass'), 
    browserSync = require('browser-sync').create(), 
    concat = require('gulp-concat'), 
    uglify = require('gulp-uglifyjs'),
    cssnano = require('gulp-cssnano'), 
    rename = require('gulp-rename'), 
    del = require('del'), 
    imagemin = require('gulp-imagemin'), 
    pngquant = require('imagemin-pngquant'), 
    cache = require('gulp-cache'), 
    autoprefixer = require('gulp-autoprefixer'), 
    include = require('gulp-html-tag-include'), 
    buffer = require('vinyl-buffer'),
    csso = require('gulp-csso'),
    merge = require('merge-stream'),
    spritesmith = require('gulp.spritesmith'),
    minify = require('gulp-minify'),
    svgSprite = require('gulp-svg-sprite');

var app = './app';
var dist = './dist';

gulp.task('scss', function() {
    return gulp.src(app + '/scss/main.scss')
        .pipe(scss())
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: false }))
        .pipe(cssnano()) //Минификация
        .pipe(concat('main.css'))
        .pipe(gulp.dest(dist + '/css'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('scripts', function() {
    return gulp.src(app + '/js/**/*')
        .pipe(concat('main.js'))
        .pipe(minify())
        .pipe(gulp.dest(dist + '/js'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: dist
        },
        notify: false
    });
    gulp.watch(app + '/scss/*.scss', ['scss']);
    gulp.watch(app + '/**/*.html', ['html-include']);
    gulp.watch(app + '/js/**/*.js', ['scripts']);
    gulp.watch(app + '/img', ['img']);
    gulp.watch(app + '/fonts', ['fonts']);
});

gulp.task('html-include', function() {
    return gulp.src(app + '/*.html')
        .pipe(include())
        .pipe(gulp.dest(dist))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('fonts', function() {
    return gulp.src(app + '/fonts/**/*')
        .pipe(gulp.dest(dist + '/fonts'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('clean', function() {
    return del.sync(dist);
});

gulp.task('img', function() {
    return gulp.src(app + '/img/**/*')
        // .pipe(cache(imagemin({ 
        //     interlaced: true,
        //     progressive: true,
        //     svgoPlugins: [{ removeViewBox: false }],
        //     use: [pngquant()]
        // })))
        .pipe(gulp.dest(dist + '/img'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('clear', function(callback) {
    return cache.clearAll();
})

gulp.task('watch', ['clear', 'clean', 'browser-sync', 'scss', 'fonts', 'html-include', 'scripts', 'img'], function() {

});

gulp.task('default', ['browser-sync', 'watch']);