jQuery(document).ready(function($) {
    $('body').on('click', '.menu-toggle', function() {
        $('#site-navigation').toggleClass('active');
        $('.menu-toggle').toggleClass('is-active');
        $('body').toggleClass('fix-body');
    })
    $('#main-slider').owlCarousel({
        dots: true,
        nav: false,
        loop: true,
        items: 1,
        margin: 0,
        autoplay:true,
        autoplayTimeout:7000
    })
    $('select').select2({});

    var buttonMenu = document.getElementById('trigger-menu');
    var fixMenu = document.getElementById('fix-nav');

    buttonMenu.onclick = function() {
        fixMenu.classList.contains("active") ? fixMenu.classList.remove("active") : fixMenu.classList.add("active");
    }

    // for reviews page
    var tabReviews = document.getElementById('tab-reviews');
    var tabDescription = document.getElementById('tab-description');
    $('body').on('click', '#tab-title-reviews a', function(e) {
        e.preventDefault();
        tabDescription.style.display = 'none'
        tabReviews.style.display = 'block';
        if (!$('#tab-title-reviews').hasClass('active')) $('#tab-title-reviews').addClass('active'); 
        $('#tab-title-description').removeClass('active');
        
    });
    $('body').on('click', '#tab-title-description a', function(e) {
        e.preventDefault();
        tabReviews.style.display = 'none'
        tabDescription.style.display = 'block';
        if (!$('#tab-title-description').hasClass('active')) $('#tab-title-description').addClass('active'); 
        $('#tab-title-reviews').removeClass('active');
    })
    // page product count
    $('body').on('click', '.plus', function() {
        var parent = $(this).parent('.quantity');
        var inputNumber = parent.find('input[type=number]')
        var valinputNumber = inputNumber.val();
        inputNumber.val( parseInt(valinputNumber) + 1);
    })
    $('body').on('click', '.minus', function() {
        var parent = $(this).parent('.quantity');
        var inputNumber = parent.find('input[type=number]')
        var valinputNumber = inputNumber.val();
        if (valinputNumber > 1) inputNumber.val( parseInt(valinputNumber) - 1);
    })
    // modal video
    $('body').on('click', '.js-video', function(e) {
        e.preventDefault();
        var video = $(this).attr('href');
        var video = video + '?enablejsapi=1&version=3&playerapiid=ytplayer';
        $('#modal-video').addClass('active');
        $('body').addClass('fix-body');
        $('#modal-video').find('iframe').attr('src', video);
        responciveVideo();
    });
    $('body').on('click', '.modal__overlay', function() {
        closeWindow();
    });
    $('body').on('click', '.modal__close', function() {
        closeWindow();
    });
    function closeWindow() {
        $('#popup-youtube-player')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');    
        $('body').removeClass('fix-body');
        $('.modal').removeClass('active');
        // $("iframe[src^='https://www.youtube.com']").pause();
    }
    function responciveVideo() {
        // responsive youtube video
        var $allVideos = $("iframe[src^='https://www.youtube.com']");
    
        $fluidEl = $("body");
    
        $allVideos.each(function() {
            $(this).data('aspectRatio', this.height / this.width)
            .removeAttr('height')
            .removeAttr('width');
    
        });
        $(window).resize(function() {
            var newWidth = $fluidEl.width()*0.9;
            if (newWidth > 800) newWidth = 800;
            $allVideos.each(function() {
                var $el = $(this);
                $el
                    .width(newWidth)
                    .height(newWidth * $el.data('aspectRatio'));
            });
    
        }).resize();
    }
    $(window).resize(function() {
        var $allVideos = $("iframe[src^='https://www.youtube.com']");
        $fluidEl = $("body");
        var newWidth = $fluidEl.width()*0.9;
        if (newWidth > 800) newWidth = 800;
        $allVideos.each(function() {
            var $el = $(this);
            $el
                .width(newWidth)
                .height(newWidth * $el.data('aspectRatio'));
        });

    }).resize();
})